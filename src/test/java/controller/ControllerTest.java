package controller;

import exceptions.InvalidMonomInputException;
import exceptions.MultipleVariablePolynomException;
import model.DivisionResultPolynom;
import model.Operation;
import model.Polynom;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by andradafarcas on 3/16/17...
 */
public class ControllerTest {

    Controller ctr;

    @Before
    public void setUp(){
        this.ctr = new Controller();
    }

    @Test
    public void testControllerAdd(){
        Polynom pol = null;
        try{
            pol= ctr.executeAction(Operation.add, "2x^3 + 4x^2 + 5x + 7", "2x^4 + 3x^2 + 9");
        }catch(InvalidMonomInputException exI){
            assertTrue(false);
        }catch(MultipleVariablePolynomException exM){
            assertTrue(false);
        }
        assertEquals(pol.toString(), "2x^4 + 2x^3 + 7x^2 + 5x + 16");


        try{
            pol= ctr.executeAction(Operation.add, "2x^3 + 4y^2 + 5x + 7", "2x^4 + 3x^2 + 9");
            assertTrue(false);
        }catch(InvalidMonomInputException exI){
            assertTrue(false);
        }catch(MultipleVariablePolynomException exM){
            assertTrue(true);
        }

        try{
            pol= ctr.executeAction(Operation.add, "2x^3 ++s2y^2 + 5x + 7", "2x^4 + 3x^2 + 9");
            assertTrue(false);
        }catch(InvalidMonomInputException exI){
            assertTrue(true);
        }catch(MultipleVariablePolynomException exM){
            assertTrue(true);
        }

    }


    @Test
    public void testControllerMultiply(){
        Polynom pol = null;
        try{
            pol= ctr.executeAction(Operation.multiply, "2x^3 + 4x^2 + 5x + 7", "2x^4 + 3x^2 + 9");
        }catch(InvalidMonomInputException exI){
            assertTrue(false);
        }catch(MultipleVariablePolynomException exM){
            assertTrue(false);
        }
        assertEquals(pol.toString(), "4x^7 + 16x^5 + 33x^3 + 8x^6 + 26x^4 + 57x^2 + 45x + 63");
    }

    @Test
    public void testControllerDerive(){
        Polynom pol = null;
        try{
            pol= ctr.executeAction(Operation.derive, "2x^3 + 4x^2 + 5x + 7", "");
        }catch(InvalidMonomInputException exI){
            assertTrue(false);
        }catch(MultipleVariablePolynomException exM){
            assertTrue(false);
        }
        assertEquals(pol.toString(), "6x^2 + 8x + 5");
    }


    @Test
    public void testControllerIntegrate(){
        Polynom pol = null;
        try{
            pol= ctr.executeAction(Operation.integrate, "2x^3 + 4x^2 + 5x + 7", "");
        }catch(InvalidMonomInputException exI){
            assertTrue(false);
        }catch(MultipleVariablePolynomException exM){
            assertTrue(false);
        }
        assertEquals(pol.toString(), "1/2x^4 + 4/3x^3 + 5/2x^2 + 7x");
    }

    @Test
    public void testControllerDivide(){
        DivisionResultPolynom pol = null;
        try{
            pol= ctr.executeDivision("2x^3 + 4x^2 + 5x + 7", "2x^4 + 3x^2 + 9");
        }catch(InvalidMonomInputException exI){
            assertTrue(false);
        }catch(MultipleVariablePolynomException exM){
            assertTrue(false);
        }
        assertEquals(pol.toString(), "0 + (2x^3 + 4x^2 + 5x + 7) / (2x^4 + 3x^2 + 9)");
    }
}
