package model;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by andradafarcas on 3/16/17...
 */
public class IntPolynomTest {
    public IntPolynom polynom;
    @Before
    public void setUp(){
        IntMonom m1 = new IntMonom(2,3);
        IntMonom m2 = new IntMonom(4,2);
        IntMonom m3 = new IntMonom(5,1);
        IntMonom m4 = new IntMonom(7,0);
        List<IntMonom> lista = new ArrayList<>();
        lista.add(m1);
        lista.add(m2);
        lista.add(m3);
        lista.add(m4);
        this.polynom = new IntPolynom(lista);
    }


    @Test
    public void testAddMonom(){
        this.polynom.addMonom(new IntMonom(2,3));
        assertTrue(this.polynom.getMonoms().get(0).getCoef() == 4);
        this.polynom.addMonom(new IntMonom(4,6));
        assertTrue(this.polynom.getMonoms().size() == 5 );
    }

    @Test
    public void testAdd(){
        IntMonom m1 = new IntMonom(2,4);
        IntMonom m2 = new IntMonom(3,2);
        IntMonom m3 = new IntMonom(9,0);
        List<IntMonom> lista = new ArrayList<>();
        lista.add(m1);
        lista.add(m2);
        lista.add(m3);
        IntPolynom pol2 = new IntPolynom(lista);

        this.polynom.add(pol2);
        assertTrue(this.polynom.getMonoms().size() == 5);
        assertTrue(this.polynom.getMonoms().get(0).getCoef() == 2);
    }

    @Test
    public void testSubtract(){
        IntMonom m1 = new IntMonom(2,4);
        IntMonom m2 = new IntMonom(3,2);
        IntMonom m3 = new IntMonom(9,0);
        List<IntMonom> lista = new ArrayList<>();
        lista.add(m1);
        lista.add(m2);
        lista.add(m3);
        IntPolynom pol2 = new IntPolynom(lista);

        this.polynom.subtract(pol2);
        assertTrue(this.polynom.getMonoms().size() == 5);
        assertTrue(this.polynom.getMonoms().get(0).getCoef() == -2);
        assertTrue(this.polynom.getMonoms().get(0).getExponent() == 4);
    }

    @Test
    public void testMultiply(){
        IntMonom m1 = new IntMonom(2,4);
        IntMonom m2 = new IntMonom(3,2);
        IntMonom m3 = new IntMonom(9,0);
        List<IntMonom> lista = new ArrayList<>();
        lista.add(m1);
        lista.add(m2);
        lista.add(m3);
        IntPolynom pol2 = new IntPolynom(lista);

        this.polynom.multiply(pol2);
        assertTrue(this.polynom.getMonoms().size() == 8);
        assertTrue(this.polynom.getMonoms().get(0).getCoef() == 4);
        assertTrue(this.polynom.getMonoms().get(0).getExponent() == 7);
    }


}
