package model;

import org.junit.Before;
import org.junit.Test;
import util.Rational;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by andradafarcas on 3/16/17...
 */
public class RationalPolynomTest {
    //same for addMonom, add,subtract as intPolynom
    public RationalPolynom polynom;
    @Before
    public void setUp(){
        RationalMonom m1 = new RationalMonom(new Rational(2,3),3);
        RationalMonom m2 = new RationalMonom(new Rational(4,7),2);
        RationalMonom m3 = new RationalMonom(new Rational(5,2),1);
        RationalMonom m4 = new RationalMonom(new Rational(7),0);
        List<RationalMonom> lista = new ArrayList<>();
        lista.add(m1);
        lista.add(m2);
        lista.add(m3);
        lista.add(m4);
        this.polynom = new RationalPolynom(lista);
    }



    @Test
    public void testMultiply(){
        RationalMonom m1 = new RationalMonom(new Rational(2,7),4);
        RationalMonom m2 = new RationalMonom(new Rational(3,4),2);
        RationalMonom m3 = new RationalMonom(new Rational(9,3),0);
        List<RationalMonom> lista = new ArrayList<>();
        lista.add(m1);
        lista.add(m2);
        lista.add(m3);
        RationalPolynom pol2 = new RationalPolynom(lista);

        this.polynom.multiply(pol2);
        assertTrue(this.polynom.getMonoms().size() == 8);
        assertTrue(this.polynom.getMonoms().get(0).getCoef().equals(new Rational(4,21)));
        assertTrue(this.polynom.getMonoms().get(0).getExponent() == 7);
    }

    @Test
    public void testDivide(){
        IntMonom m1 = new IntMonom(2,3);
        IntMonom m2 = new IntMonom(4,2);
        IntMonom m3 = new IntMonom(5,1);
        IntMonom m4 = new IntMonom(7,0);
        List<IntMonom> lista = new ArrayList<>();
        lista.add(m1);
        lista.add(m2);
        lista.add(m3);
        lista.add(m4);
        IntPolynom polynom = new IntPolynom(lista);

        IntMonom m1_1 = new IntMonom(2,4);
        IntMonom m2_1 = new IntMonom(3,2);
        IntMonom m3_1 = new IntMonom(9,0);
        List<IntMonom> lista2 = new ArrayList<>();
        lista2.add(m1_1);
        lista2.add(m2_1);
        lista2.add(m3_1);
        IntPolynom polynom2 = new IntPolynom(lista2);

        RationalPolynom pol1 = new RationalPolynom(polynom);
        RationalPolynom pol2 = new RationalPolynom(polynom2);

        System.out.println(pol2.getMonoms().size());

        DivisionResultPolynom dvs = pol1.divide(pol2);

        assertTrue(dvs.divisor.getMonoms().size() == pol2.getMonoms().size());
        assertTrue(dvs.quotient.getMonoms().size() == 0);
        assertTrue(dvs.remainder.getMonoms().size() == 4);

    }
}
