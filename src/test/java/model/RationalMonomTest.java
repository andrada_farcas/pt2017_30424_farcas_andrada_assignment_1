package model;

import exceptions.DifferentExponentException;
import org.junit.Before;
import org.junit.Test;
import util.Rational;

import static org.junit.Assert.assertTrue;

/**
 * Created by andradafarcas on 3/16/17...
 */
public class RationalMonomTest {
    public RationalMonom monom;

    @Before
    public void setUp(){
        this.monom = new RationalMonom(new Rational(2,3),2);
    }

    @Test
    public void gettersSet(){
        assertTrue(monom.getCoef().numerator() == 2);
        assertTrue(monom.getCoef().denominator() == 3);
        assertTrue(monom.getExponent() == 2);
    }



    @Test
    public void setNegativeTest() {
        monom.setNegative();
        assertTrue(monom.getCoef().equals(new Rational(-2,3)));
    }

    @Test
    public void positiveTest(){
        assertTrue(monom.positive());
    }


    @Test
    public void testAdd(){
        try{
            System.out.println(monom.getCoef());

            monom.add(new RationalMonom(new Rational(7,2),2));
            System.out.println(monom.getCoef());

            assertTrue(monom.getCoef().equals(new Rational(25,6)));
        }catch (DifferentExponentException ex){
            assertTrue(false);
        }


        try{
            monom.add(new RationalMonom(new Rational(2,2),4));
            assertTrue(false);
        }catch (DifferentExponentException ex){
            assertTrue(true);
        }
    }



    @Test
    public void testSubtract(){
        try{
            monom.subtract(new RationalMonom(new Rational(1,3),2));
            assertTrue(true);
            assertTrue(monom.getCoef().equals(new Rational(1,3)));
        }catch (DifferentExponentException ex){
            assertTrue(false);
        }

        try{
            monom.subtract(new RationalMonom(new Rational(2,4),7));
            assertTrue(false);
        }catch (DifferentExponentException ex){
            assertTrue(true);
        }
    }


    @Test
    public void testMultiply(){
        monom.multiply(new RationalMonom(new Rational(3,2),2));
        assertTrue(monom.getCoef().equals(new Rational(1)));
        assertTrue(monom.getExponent() == 4);

        setUp();
        monom.multiply(new RationalMonom(new Rational(5,8),4));
        assertTrue(monom.getCoef().equals(new Rational(5,12)));
        assertTrue(monom.getExponent() == 6);
    }



    @Test
    public void testDerive(){
        monom.derive();
        assertTrue(monom.getCoef().equals(new Rational(4,3)));
        assertTrue(monom.getExponent() == 1);

        monom.derive();
        assertTrue(monom.getCoef().equals(new Rational(4,3)));
        assertTrue(monom.getExponent() == 0);

        monom.derive();
        assertTrue(monom.getCoef().equals(0));
        assertTrue(monom.getExponent() == 0);
    }

    @Test
    public void testDivide(){
        this.monom = new RationalMonom(new Rational(2,3), 6);
        this.monom.divide(new RationalMonom(new Rational(5,6),2));

        assertTrue(this.monom.getCoef().equals(new Rational(4,5)));
        assertTrue(this.monom.getExponent() == 4);
    }

    @Test
    public void testIntegrate(){
        monom.integrate();
        assertTrue(monom.getExponent() == 3);
        assertTrue(monom.getCoef().equals(new Rational(2,9)));
    }
}
