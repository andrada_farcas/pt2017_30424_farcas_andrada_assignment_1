package model;

import exceptions.DifferentExponentException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by andradafarcas on 3/16/17...
 */
public class IntMonomTest {
    public IntMonom monom;

    @Before
    public void setUp(){
        this.monom = new IntMonom(12,2);
    }

    @Test
    public void gettersSet(){
        assertTrue(monom.getCoef() == 12);
        assertTrue(monom.getExponent() == 2);
    }

    @Test
    public void setNegativeTest() {
        monom.setNegative();
        assertTrue(monom.getCoef() == -12);
    }

    @Test
    public void positiveTest(){
        assertTrue(monom.positive());
    }

    @Test
    public void testAdd(){
        try{
            monom.add(new IntMonom(2,2));
            assertTrue(true);
            assertTrue(monom.getCoef() == 14);
        }catch (DifferentExponentException ex){
            assertTrue(false);
        }

        try{
            monom.add(new IntMonom(12,8));
            assertTrue(false);
        }catch (DifferentExponentException ex){
            assertTrue(true);
        }
    }

    @Test
    public void testSubtract(){
        try{
            monom.subtract(new IntMonom(2,2));
            assertTrue(true);
            assertTrue(monom.getCoef() == 10);
        }catch (DifferentExponentException ex){
            assertTrue(false);
        }

        try{
            monom.subtract(new IntMonom(2,3));
            assertTrue(false);
        }catch (DifferentExponentException ex){
            assertTrue(true);
        }
    }

    @Test
    public void testMultiply(){
        monom.multiply(new IntMonom(2,2));
        assertTrue(monom.getCoef() == 24);
        assertTrue(monom.getExponent() == 4);

        setUp();
        monom.multiply(new IntMonom(3,4));
        assertTrue(monom.getCoef() == 36);
        assertTrue(monom.getExponent() == 6);
    }

    @Test
    public void testDerive(){
        monom.derive();
        assertTrue(monom.getCoef() == 24);
        assertTrue(monom.getExponent() == 1);

        monom.derive();
        assertTrue(monom.getCoef() == 24);
        assertTrue(monom.getExponent() == 0);

        monom.derive();
        assertTrue(monom.getCoef() == 0);
        assertTrue(monom.getExponent() == 0);
    }


}
