package view;

/**
 * Created by andradafarcas on 3/12/17...
 */

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import controller.Controller;
import model.*;
import exceptions.InvalidMonomInputException;
import exceptions.MultipleVariablePolynomException;


public class View {
    Controller controller;

    BorderPane borderPane;

    TextField polynom1;
    TextField polynom2;
    TextField resultField;

    Button buttonExit;


    Button buttonAdd;
    Button buttonSubtract;
    Button buttonMultiply;
    Button buttonDivide;

    Button buttonDerivate;
    Button buttonIntegrate;



    @SuppressWarnings("rawtypes")
    public View(){

        controller = new Controller();

        this.polynom1 = new TextField();
        this.polynom1.setPromptText("First polynomial: ");

        this.polynom2 = new TextField();
        this.polynom2.setPromptText("Second polynomial: ");

        this.resultField = new TextField();
        this.resultField.setPromptText("Result ");

        this.buttonAdd = new Button("Add");
        this.buttonSubtract = new Button("Subtract");
        this.buttonMultiply = new Button("Multiply");
        this.buttonDivide = new Button("Divide");
        this.buttonDerivate = new Button("Derive");
        this.buttonIntegrate = new Button("Integrate");

        this.buttonExit = new Button("Exit");

        this.initBorderPane();
    }

    public BorderPane getView(){
        return this.borderPane;
    }

    private void initBorderPane(){
        this.borderPane = new BorderPane();

        borderPane.setPadding(new Insets(10,20,10,20));

        VBox firstP = new VBox(new Label("First Polynomial: "),polynom1);
        VBox secondP = new VBox(new Label("Second Polynomial: "),polynom2);

        HBox buttons = new HBox(10, buttonAdd,buttonSubtract,buttonMultiply,buttonDivide,buttonDerivate,buttonIntegrate);

        VBox vboxCenter = new VBox(10,firstP,secondP,buttons,resultField);
        vboxCenter.setAlignment(Pos.TOP_RIGHT);

        vboxCenter.setMargin(resultField, new Insets(40,0,0,0));

        this.borderPane.setCenter(vboxCenter);

        HBox hBox = new HBox(10,buttonExit);
        hBox.setAlignment(Pos.CENTER_RIGHT);

        this.buttonAdd.setOnAction(event -> {
            executeAction(Operation.add);
        });

        this.buttonSubtract.setOnAction(event -> {
            executeAction(Operation.subtract);
        });

        this.buttonMultiply.setOnAction(event -> {
            executeAction(Operation.multiply);
        });

        this.buttonDivide.setOnAction(event -> {
            executeAction(Operation.divide);
        });

        this.buttonDerivate.setOnAction(event -> {
            executeAction(Operation.derive);
        });

        this.buttonIntegrate.setOnAction(event -> {
            executeAction(Operation.integrate);
        });

        this.buttonExit.setOnAction(event -> {
            Stage stage = (Stage) buttonExit.getScene().getWindow();
            // do what you have to do
            stage.close();
        });

        this.borderPane.setBottom(hBox);
    }


    public void executeAction(Operation op){
        try{
            if(op != Operation.divide) {
                Polynom p = controller.executeAction(op, this.polynom1.getText().toString(), this.polynom2.getText().toString());
                resultField.setText(p.toString());
            }else{
                DivisionResultPolynom p = controller.executeDivision(this.polynom1.getText().toString(), this.polynom2.getText().toString());
                resultField.setText(p.toString());
            }
        }catch(MultipleVariablePolynomException ex) {
            resultField.setText(ex.toString());
            return;
        }catch(InvalidMonomInputException ex2){
            resultField.setText(ex2.toString());
            return;
        }
    }
}