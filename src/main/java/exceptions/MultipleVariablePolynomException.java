package exceptions;

/**
 * Created by andradafarcas on 3/7/17.
 */
public class MultipleVariablePolynomException extends Exception {
    private String s1;

    public MultipleVariablePolynomException(String s2) {
        s1 = s2;
    }
    @Override
    public String toString() {
        return s1;
    }
}
