package model;

import exceptions.DifferentExponentException;
import util.Rational;

/**
 * Created by andradafarcas on 3/12/17...
 */


public class RationalMonom extends Monom<Rational>{

    //creates and initializes a rational monom when coefficient and exponent given
    public RationalMonom(Rational coef, int exp) {
        super(coef,exp);
    }

    //copy constructor
    public RationalMonom(RationalMonom m) {
        super(m);
        this.coefficient = m.coefficient;
        this.exponent = m.exponent;
    }


    //checks whether the monom is positive or not
    public boolean positive(){
        return this.coefficient.toDouble() > 0;
    }

    //sets the monom's sign to negative
    public void setNegative(){
        this.coefficient = this.coefficient.negate();
    }

    //integrates the monom
    public void integrate(){
        this.exponent++;
        this.coefficient = this.coefficient.divides(new Rational(this.exponent));
    }


    //adds the rational monom m to the current monom
    //throws DifferentExponentException if they are of different exponent
    public void add(Monom<Rational> m) throws DifferentExponentException{
        checkIfSameExponent(m);
        if(m.getClass() == RationalMonom.class)
            this.coefficient = this.coefficient.plus(m.getCoef());
        else
            throw new DifferentExponentException("erroar");
    }

    //subtracts the rational monom m from the current monom
    //throws DifferentExponentException if they are of different exponent
    public void subtract(Monom<Rational> m) throws DifferentExponentException{
        checkIfSameExponent(m);
        this.coefficient = this.coefficient.minus(m.coefficient);
    }

    //multiplies the rational monom m with the current monom
    public void multiply(Monom<Rational> m){
        this.coefficient = this.coefficient.times(m.coefficient);
        this.exponent += m.exponent;
    }


    //divides the current monom with the rational monom m
    public void divide(Monom<Rational> m){
        this.coefficient = this.coefficient.divides(m.coefficient);
        this.exponent -= m.exponent;
    }

    //derives the current monom
    public void derive(){
        this.coefficient = this.coefficient.times(new Rational(this.exponent));
        if(this.exponent != 0)
            this.exponent--;
    }
}
