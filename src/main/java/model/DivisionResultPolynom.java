package model;


/**
 * Created by andradafarcas on 3/12/17...
 */



public class DivisionResultPolynom {
    RationalPolynom quotient;
    RationalPolynom remainder;
    RationalPolynom divisor;

    public DivisionResultPolynom(RationalPolynom quotient, RationalPolynom remainder, RationalPolynom divisor) {
        this.quotient = quotient;
        this.remainder = remainder;
        this.divisor = divisor;
    }


    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(this.quotient.toString());
        if (this.remainder.getMonoms().size() != 0) {
            sb.append(" + (");
            sb.append(this.remainder.toString());
            sb.append(") / (");
            sb.append(this.divisor);
            sb.append(")");
        }
        return sb.toString();
    }
}
