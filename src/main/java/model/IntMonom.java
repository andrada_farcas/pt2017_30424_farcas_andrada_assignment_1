package model;

import exceptions.DifferentExponentException;

/**
 * Created by andradafarcas on 3/7/17.
 */


public class IntMonom extends Monom<Integer> {
    //Monom with an Integer type coefficient


    //Constructor -> Constructs a Monom when coef and exp are given
    public IntMonom(Integer coef, int exp) {
        super(coef,exp);
    }


    //copy Constructor
    public IntMonom(Monom m) {
        super(m);
    }


    public  boolean positive(){
        return this.coefficient > 0;
    }


    public  void setNegative(){
        this.coefficient = -this.coefficient;
    }


    public  void integrate(){
        /*
        implemented in RationalMonom as the integration result
        is not an monom with Integer coefficient
         */
    }

    public void add(Monom<Integer> m) throws DifferentExponentException{
        //we check if the monoms' exponents have the same value
        //if exponents differ, a DifferentExponentException is thrown
        checkIfSameExponent(m);
        this.coefficient+= m.coefficient ;
    }

    public  void subtract(Monom<Integer> m) throws DifferentExponentException{
        //we check if the monoms' exponents have the same value
        //if exponents differ, a DifferentExponentException is thrown
        checkIfSameExponent(m);
        this.coefficient -= m.coefficient;
    }


    public  void multiply(Monom<Integer> m){
        this.coefficient *= m.coefficient;
        this.exponent += m.exponent;
    }


    public  void divide(Monom<Integer> m){
        /*
        implemented in RationalMonom as the division result
        is not an monom with Integer coefficient
         */
    }

    public void derive(){
        this.coefficient *= this.exponent;
        if(exponent!=0)
            this.exponent--;
    }
}