package model;

import exceptions.DifferentExponentException;

import java.util.Comparator;

/**
 * Created by andradafarcas on 3/7/17.
 */

public abstract class Monom<T> {
    /*
    Monom generic abstract class.
     */

    protected int exponent;
    protected T coefficient;

    public Monom(T coef, int exp) {
        //Constructor - coefficient and exponent given
        this.coefficient = coef;
        this.exponent = exp;
    }

    public Monom(Monom m) {
        //Copy constructor
        this.coefficient = (T) m.getCoef();
        this.exponent = m.getExponent();
    }

    public T getCoef() {
        return this.coefficient;
    }

    public int getExponent() {
        return this.exponent;
    }

    public String toString() {
        //returns a formatted monom String from coefficient and exponent.
        if (coefficient.equals(0))
            return "";
        if (exponent == 0)
            return coefficient.toString();

        if (coefficient.equals(1))
            if (exponent == 1)
                return "x";

        if (coefficient.equals(-1))
            if (exponent == 1)
                return "-x";

        if (coefficient.equals(1))
            return "x" + "^" + exponent;

        if (coefficient.equals(-1))
            return "-x" + "^" + exponent;

        if (exponent == 1)
            return coefficient.toString() + "x";

        return coefficient.toString() + "x^" + exponent;
    }

    public static Comparator<Monom> cmpByExp = (Monom m1, Monom m2) -> {
        //compares two monoms by their exponent. (for sorting function)

        // For descending order( - compare):
        // returns 1 if m1.exp < m2.exp
        // returns 0 if m1.exp = m2.exp
        // returns -1 if m1.exp > m2.ex

        return -Integer.compare(m1.getExponent(), m2.getExponent());
    };

    protected void checkIfSameExponent(Monom m) throws DifferentExponentException {
        //checks if monom has the same exponent as monom m recieved by param.
        if (this.exponent != m.exponent)
            throw new DifferentExponentException("Monoms have different! ");
    }

    //checks if a monom is positive - abstract needed because we use Rational class;
    public abstract boolean positive();

    //changes the sign of the coefficient - abstract needed because we use Rational class;
    public abstract void setNegative();

    //integration operation - abstract needed because we use Rational class;
    public abstract void integrate();

    //derivation operation - abstract needed because we use Rational class;
    public abstract void derive();

    //add operation. Adds monom m to the current monom
    public abstract void add(Monom<T> m) throws DifferentExponentException;

    //subtract operation. Subtracts monom m from the current monom
    public abstract void subtract(Monom<T> m) throws DifferentExponentException;

    //multiplication operation. Multiplies monom m with the current monom
    public abstract void multiply(Monom<T> m);

    //division operation. Divides current monom to monom m
    public abstract void divide(Monom<T> m);

}