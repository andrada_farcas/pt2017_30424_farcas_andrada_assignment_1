package model;

/**
 * Created by andradafarcas on 3/12/17...
 */

public enum Operation {
    add, subtract, multiply, divide, integrate, derive
}
