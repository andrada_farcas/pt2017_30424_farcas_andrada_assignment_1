package model;

import java.util.List;


/**
 * Created by andradafarcas on 3/12/17...
 */



public class IntPolynom extends Polynom<IntMonom> {


    //constructor - creates and initializes an int polynom from a list of int monoms
    public IntPolynom(List<IntMonom> monoms) {
        super(monoms);
    }

    //copy constructor
    public IntPolynom(Polynom<IntMonom> p2) {
        this.monoms = p2.getMonoms();
    }

    //constructor - creates and initializes an intPolynom
    public IntPolynom(){
        super();
    }

    //copy constructor
    public IntPolynom(IntPolynom p){
        super(p);
    }


    @Override
    public DivisionResultPolynom divide(Polynom<IntMonom> p2) {
        return null;
        //division - operation is to be implemented in RationalPolynom
    }

    //Polynomial multiplication operation
    @Override
    public void multiply(Polynom<IntMonom> p) {

        //creates a new empty polynom that will build the result
        IntPolynom res = new IntPolynom();

        //multiplies each monom from current polynom with each monom of the polynom p
        for(IntMonom m1 : this.monoms) {
            for (IntMonom m2 : p.getMonoms()) {
                IntMonom temp = new IntMonom(m1);
                temp.multiply(m2);
                res.addMonom(temp);
            }
        }
        this.monoms = res.monoms;
    }

}