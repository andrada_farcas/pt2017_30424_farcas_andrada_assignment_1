package model;

import util.Rational;

import java.util.List;

public class RationalPolynom extends Polynom<RationalMonom>{

    //constructor -> creates and initializes a rational polynom from an int polynom
    public RationalPolynom(IntPolynom p1){
        super();
        for(IntMonom intM:p1.getMonoms()){
            this.addMonom(new RationalMonom(new Rational(intM.getCoef()),intM.getExponent()));
        }
    }


    //copy constructor
    public RationalPolynom(RationalPolynom p){
        super(p);
    }

    public RationalPolynom(){
        super();
    }


    //constructor -> creates and initializes a rational polynom from a list of rational monoms
    public RationalPolynom(List<RationalMonom> monoms) {
        super(monoms);
    }

    //copy constructor
    public RationalPolynom(Polynom<RationalMonom> p2) {
        this.monoms = p2.getMonoms();
    }


    @Override
    public DivisionResultPolynom divide(Polynom<RationalMonom> p2) {
        //quotient polynom, it is formed while dividing
        RationalPolynom quotient = new RationalPolynom();

        //create a new Polynom that will store the changing value of the remainng divident (after subtractions)
        RationalPolynom remDivident = new RationalPolynom(this.monoms);

        //the first monom of the divisor
        RationalMonom firstMonDivisor = null;
        if(p2.getMonoms().size()>0)
            firstMonDivisor = p2.getMonoms().get(0);


        //while first monom's exponent of the divident polynom is greater or equal than the first monom's exponent of the divisor polynom
        while(remDivident.monoms.size() > 0 && remDivident.monoms.get(0).getExponent() >= firstMonDivisor.getExponent()){

            //a copy of the first monom of the Remaning Divident is created
            RationalMonom remCopy = new RationalMonom(remDivident.getMonoms().get(0));


            //divide with the first monom of the divisor
            remCopy.divide(firstMonDivisor);


            //add the last resulted monom to the quotient
            quotient.addMonom(remCopy);


            //creates an empty polynom
            RationalPolynom rightSideProduct = new RationalPolynom();


            //rightSideProduct becomes the polynom containing remCopy, required for multiplication operation
            rightSideProduct.addMonom(remCopy);


            //rightSideProduct stores the last added monom of the quotient multiplied with the divisor
            rightSideProduct.multiply(p2);


            //subtracts the right side product from the remDivident
            remDivident.subtract(rightSideProduct);
        }

        //creates a rational polynom from the int divisor given as parameter
        RationalPolynom pol = new RationalPolynom(p2);

        //returns a DivisionResultPolynom, storing the quotient, the remainder and the divisor.
        return new DivisionResultPolynom(quotient, remDivident, pol);
    }


    //multiplication operation
    @Override
    public void multiply(Polynom<RationalMonom> p) {

        RationalPolynom res = new RationalPolynom();

        for(RationalMonom m1 : this.monoms) {
            for (RationalMonom m2 : p.getMonoms()) {
                RationalMonom temp = new RationalMonom(m1);
                temp.multiply(m2);
                res.addMonom(temp);
            }
        }
        this.monoms = res.monoms;
    }


}