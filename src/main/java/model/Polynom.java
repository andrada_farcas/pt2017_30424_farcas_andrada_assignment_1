package model;

import exceptions.DifferentExponentException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 *
 *  Created by andradafarcas on 3/7/17...
 *
 */

public abstract class Polynom<T extends Monom> {

    //Abstract Generic Polynom
    //It can only hold data of Monom type (IntMonom, RationalMonom)

    List<T> monoms;

    //constructor - initializes monom list
    public Polynom(){
        this.monoms = new ArrayList<>();
    }

    //copy constructor
    public Polynom(Polynom<T> p){
        this.monoms = new ArrayList<>();
        this.monoms = p.getMonoms();
    }

    //creates and initializes a polynom from a list of monoms
    public Polynom(List<T> monoms) {
        this.monoms = new ArrayList<>();
        this.monoms = monoms;
        //sorts the monoms of the polynom in descending order of their expoent
        Collections.sort(monoms, Monom.cmpByExp);
    }


    //adds a monom to the current list of monoms
    protected void addMonom(T m) {
        for (Monom mCrt : monoms) {
            if (mCrt.getExponent() == m.getExponent()) {
                try {
                    mCrt.add(m);
                } catch (DifferentExponentException ex) {
                    ex.printStackTrace();
                }
                return;
            }
        }
        this.monoms.add(m);
    }


    //returns the list of monoms
    public List<T> getMonoms(){

        return this.monoms;
    }


    //transforms the polynom in a formatted string
    public String toString(){
        //if the polynom has no monoms, that means that is equal to 0
        if(monoms.size()==0){
            return "0";
        }

        StringBuilder sb = new StringBuilder();

        //boolean to check if first position
        boolean first = true;

        //appends " + " between monoms, in case they are positive and not the first
        for(Monom m:monoms){
            if(m.positive()){
                if(!first)
                    sb.append(" + ");
                sb.append(m.toString());
            }else{
                sb.append(m.toString());
            }
            first = false;
        }

        return sb.toString();
    }

    //addition operation using executeOperation method
    public void add(Polynom<T> p){
        executeOperation(p, Operation.add);
    }

    //subtraction operation using executeOperation method
    public void subtract(Polynom<T> p){
        executeOperation(p, Operation.subtract);
    }

    //integration operation
    public void integrate(){
        for(Monom dm : monoms){
            dm.integrate();
        }
    }

    //division operation returning a new object containing quotient, remainder and divisor
    public abstract DivisionResultPolynom divide(Polynom<T> p2);

    //executes addition or subtraction (depending on given operation parameter) of polynom p to/ from the current polynom
    private void executeOperation(Polynom<T> p, Operation op){

        //creates a list of monoms that will store the result
        List<T> res = new ArrayList<T>();

        int i=0, j=0;

        //loops between all monoms of current polynom and p polynom
        while(i<monoms.size() && j<p.monoms.size()){
            try{

                //if exponents are equal, the addition operation between the two is executed
                if(op == Operation.add)
                    monoms.get(i).add(p.monoms.get(j));

                //if exponents are equal, the  subtraction operation between the two is executed
                else if(op == Operation.subtract)
                    monoms.get(i).subtract(p.monoms.get(j));

                //if a computed monom has the coefficient 0, it will not be added to the result
                if(! monoms.get(i).getCoef().equals(0))
                    res.add(monoms.get(i));
                i++;
                j++;

                //if the exponents are different, DifferentExponentException is thrown
            }catch (DifferentExponentException e){

                //check if the current monom from the current polynom is greater than the current monom of polynom p
                if(monoms.get(i).getExponent() > p.monoms.get(j).getExponent()){

                    //if true, the monom from ith position is added to the result
                    res.add(monoms.get(i));
                    i++;
                }else{

                    //if false, the negative monom from jth position is added to the result
                    if(op == Operation.subtract)
                        p.monoms.get(j).setNegative();
                    res.add(p.monoms.get(j));
                    j++;
                }
            }
        }

        //checks if there are monoms left from the current polynom after the while condition has been executed
        //if true, they are appended to the result
        while(i<monoms.size()){
            res.add(monoms.get(i));
            i++;
        }

        //checks if there are monoms left from the polynom p after the while condition has been executed
        //if true, they are appended to the result is the addition operation is made (negative if the operation is subtraction)
        while(j<p.monoms.size()){
            if(op == Operation.subtract)
                p.monoms.get(j).setNegative();
            res.add(p.monoms.get(j));
            j++;
        }

        this.monoms = res;
    }

    //multiplication operation
    public abstract void multiply(Polynom<T> p);

    //derivation operation
    public void derivate(){

        //iterator is used for removing monoms from the monom list while iterating them
        Iterator<T> it = monoms.iterator();
        while(it.hasNext()){
            //m is the current monom in iteration
            T m = it.next();

            //derive the monom m
            m.derive();

            //if after the derivation the coef != 0, the monom is removed from the monoms list
            if(m.getCoef().equals(0))
                it.remove();
        }
    }


}