package controller;


import model.*;
import exceptions.InvalidMonomInputException;
import exceptions.MultipleVariablePolynomException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andradafarcas on 3/12/17.
 */
public class Controller {


    public static IntMonom getMonomFromString(String monom) throws InvalidMonomInputException{

        // blank spaces are eliminated from the input string
        String myMonom = monom.replaceAll("\\s+", "");

        //defining the form of the monom
        String patternMonom = "[+-]?\\d*[a-z](\\^\\d+)?";
        //pattern -> (sign) + (coef) + variableName + (^ + exponent)       () = optional

        // d - digit (0-9)
        // ? - optional
        // * - 0 or more
        // + - 1 or more
        // \\ - escape, for special characters


        //check monom
        if (myMonom.matches(patternMonom)) {
            String patternCuSemn = "[+-]\\d*[a-z](\\^\\d+)?";
            if (myMonom.matches(patternCuSemn)) {
                //has the form: +-(coeff)x(^exp)

                IntMonom res = calculateMonom(myMonom.substring(1)); //computes the monom without its sign

                boolean positive = myMonom.charAt(0) == '+'; //determines if the monom is positive or negative by checking its sign
                if (positive) {
                    return res;
                }
                res.setNegative();
                return res;
            }else{
                //it has no sign - has the form: (coeff)x(^exp)

                return calculateMonom(myMonom);
            }
        }else if(myMonom.matches("[+-]?[0-9]+")){
            //is a constant - has the form: (sign)coeff
            boolean negative = false;

            //checks whether the monom has a sign or not. If it has no sign, it is by default positive.
            if(myMonom.charAt(0) == '+' || myMonom.charAt(0) == '-'){
                //assigns negative true if the sign in front of the monom is "-"
                negative = myMonom.charAt(0) == '-';
                //once the sign stored -> remove the sign from the string
                myMonom = myMonom.substring(1);
            }
            //stores the int value converted from the string
            int coef = Integer.parseInt(myMonom);

            //returns the monom
            if(negative)
                return new IntMonom(-coef, 0);
            return new IntMonom(coef, 0);
        }else{
            throw new InvalidMonomInputException("WRONG PATTERN EXCEPTION " + monom);
        }
    }

    private static IntMonom calculateMonom(String monom) {
        //computes an integer monom from an unsigned string having the form: (coeff)x(^exp)

        // x -> (1,x,1) (rez.length = 0)
        String[] rez = monom.split("[a-z]");
        if (rez.length == 0) {
            return new IntMonom(1, 1);
        }

        // cx -> (c,x,1) (rez.length = 1)
        if (rez.length == 1) {
            int coef = Integer.parseInt(rez[0]);
            return new IntMonom(coef, 1);
        }

        // x^exp -> (1,x,exp) (rez.length = 2 && isEmpty(rez[1]))
        int coef;
        if(rez[0].isEmpty()){
            coef = 1;
        }else{
            coef = Integer.parseInt(rez[0]);
        }

        // cx^exp -> (c,x,exp) (rez.length = 2)
        int exp = Integer.parseInt(rez[1].substring(1));
        return new IntMonom(coef, exp) {
        };
    }

    public static List<IntMonom> getMonomListFromString(String myPolynom) throws MultipleVariablePolynomException, InvalidMonomInputException {
        //computes the integer monom list from a string

        List<IntMonom> monoms = new ArrayList<>();
        String polynom = myPolynom.replaceAll("\\s+", ""); //remove spaces

        // ?= - look behind expression(left/previous)
        //splits the string after sign storing the sign with it
        String[] res = polynom.split("(?=[+-])");
        // e.g.: str = " 1x^2+2x^3-3x^4 "
        //     str.split("(?=[+-])") =>   1x^2 ; +2x^3 ; -3x^4

        //loops through the splitted monom strings from the polynom string
        for(String temp:res){
            IntMonom mon = getMonomFromString(temp);
            //adds the monom to the list if its coefficient is non negative
            if(mon.getCoef()!=0)
                monoms.add(mon);
        }

        //checks if the polynom has only one variableName
        String patternSameVariable = "[0-9^+-]*([a-z])[0-9^+-]*(\\1[0-9^+-]*)*";
        if(!polynom.matches(patternSameVariable) && !polynom.matches("^[+\\-0-9]+$")){
            throw new MultipleVariablePolynomException("Polynom of multiple variable! ");
        }

        return monoms;
    }


    public Polynom executeAction(Operation op, String p1, String p2) throws MultipleVariablePolynomException, InvalidMonomInputException{
        // executes an operation and returns the resulted polynom

        //tries to get a polynom from a given string - if not, proper exceptions are thrown
        List<IntMonom> intMonoms1 = null;
        try{
            intMonoms1 = getMonomListFromString(p1);
        }catch(MultipleVariablePolynomException ex){
            throw new MultipleVariablePolynomException("Error! First polynom is of multiple variable! ");
        }catch(InvalidMonomInputException ex2){
            throw new InvalidMonomInputException("Error! First polynom is invalid!");
        }

        //instantiates polynom 1
        IntPolynom polynom1 = new IntPolynom(intMonoms1);

        switch (op) {
            case derive:
                polynom1.derivate();
                return polynom1;
            case integrate:
                RationalPolynom rationalP = new RationalPolynom(polynom1);
                rationalP.integrate();
                return rationalP;
        }

        List<IntMonom> intMonoms2 = null;
        try{
            intMonoms2 = getMonomListFromString(p2);
        }catch(MultipleVariablePolynomException ex){
            throw new MultipleVariablePolynomException("Error! Second polynom is of multiple variable!");
        }catch(InvalidMonomInputException ex2){
            throw new InvalidMonomInputException("Error! Second polynom is invalid!");
        }

        //instantiates second polynom
        IntPolynom polynom2 = new IntPolynom(intMonoms2);

        switch (op){
            case add:
                polynom1.add(polynom2);
                return polynom1;
            case subtract:
                polynom1.subtract(polynom2);
                return polynom1;
            case multiply:
                polynom1.multiply(polynom2);
                return polynom1;
            // obs: case .divide missing as it returns a DivisionResultPolynom, not Polynom
            //      treated separately.
        }
        return null;
    }

    public DivisionResultPolynom executeDivision(String p1, String p2) throws MultipleVariablePolynomException, InvalidMonomInputException{
        //executes the division operation


        //tries to get a polynom from a given string - if not, proper exceptions are thrown
        List<IntMonom> intMonoms1 = null;
        try{
            intMonoms1 = getMonomListFromString(p1);
        }catch(MultipleVariablePolynomException ex){
            throw new MultipleVariablePolynomException("Error! First polynom is of multiple variable! ");
        }catch(InvalidMonomInputException ex2){
            throw new InvalidMonomInputException("Error! First polynom is invalid!");
        }

        //instantiates polynom 1
        IntPolynom polynom1 = new IntPolynom(intMonoms1);


        //tries to get a polynom from a given string - if not, proper exceptions are thrown
        List<IntMonom> intMonoms2 = null;
        try{
            intMonoms2 = getMonomListFromString(p2);
        }catch(MultipleVariablePolynomException ex){
            throw new MultipleVariablePolynomException("Error! Second polynom is of multiple variable!");
        }catch(InvalidMonomInputException ex2){
            throw new InvalidMonomInputException("Error! Second polynom is invalid!");
        }

        IntPolynom polynom2 = new IntPolynom(intMonoms2);


        //creates a rational polynom from the first int polynom
        RationalPolynom pRational = new RationalPolynom(polynom1);


        //creates a rational polynom from the second int polynom
        //divides the polynom to the second polynom
        DivisionResultPolynom div = pRational.divide(new RationalPolynom(polynom2));

        //returns a DivisionResultPolynom which stores the quotient, divisor, remainder
        return div;
    }

}